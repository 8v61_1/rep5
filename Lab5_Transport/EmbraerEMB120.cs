﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class EmbraerEMB120 : Airplane
    {
        public EmbraerEMB120(double distance)
        {
            TravelDistance = distance;
            TransportName = "EmbraerEMB120";
            ComfortClass = "First";
            PricePerKm = 5;
            Speed = 552;
            BaggagePricePerKg = 20;
            MinimumDistance = 200;
            DepartureTime = 25;
            PlaneSizeType = "Regional airliner";
        }
    }
}
