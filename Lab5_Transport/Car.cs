﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class Car : AbstractTransport
    {
        private double _fuelTankCapacityKm;
        private string _carClass;

        public double FuelTankCapacityKm
        {
            get { return _fuelTankCapacityKm; }
            set { _fuelTankCapacityKm = value; }
        }
        public string CarClass
        {
            get { return _carClass; }
            set { _carClass = value; }
        }
        public virtual double CountTimeToRefuel()
        {
            double OneTimeToRefuel = 1;
            return OneTimeToRefuel * (TravelDistance / FuelTankCapacityKm);
        }
    }
}
