﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class Sapsan : Train
    {
        public Sapsan(double distance)
        {
            TravelDistance = distance;
            TransportName = "Sapsan";
            ComfortClass = "Business";
            PricePerKm = 4;
            Speed = 200;
            BaggagePricePerKg = 7;
            Manufacturer = "Siemens AG";
            NumberOfTrainCars = 10;
            MinimumDistance = 300;
            DepartureTime = 30;
        }
    }
}
