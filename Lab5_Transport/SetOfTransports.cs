﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace lab3_Routes
{
    public class SetOfTransports
    {
        private List<AbstractTransport> _transports = new List<AbstractTransport>();
        
        public List<AbstractTransport> Transports
        {
            get { return _transports; }
            set { _transports = value; }
        }


        public double CountPrice(double baggage)
        {
            double price = 0;
            foreach (AbstractTransport transport in Transports)
            {
                double individualPrice = 0;
                individualPrice = transport.PricePerKm * transport.TravelDistance;//цена за дальность
                individualPrice = individualPrice + transport.BaggagePricePerKg * baggage;//+ цена на багаж
                string comfort = transport.ComfortClass;
                switch (comfort)
                {
                    case "Economy":
                        break;
                    case "Business":
                        individualPrice = individualPrice * 1.2;
                        break;
                    case "First":
                        individualPrice = individualPrice * 1.4;
                        break;
                }
                price = price + individualPrice;
            }
            return price;
        }

        public double CountTime()
        {
            double time = 0;
            for (int i = 0; i < Transports.Count(); i++)
            {
                if (Transports[i] is Car)
                {
                    Car transport = (Car)Transports[i];
                    double individualTime = 0;
                    individualTime = transport.TravelDistance / transport.Speed;
                    individualTime = individualTime + transport.CountTimeToRefuel() / 60;
                    time = time + individualTime;
                }
                if (Transports[i] is AbstractLongDistanceTransport)
                {
                    AbstractLongDistanceTransport transport = (AbstractLongDistanceTransport)Transports[i];
                    double individualTime = 0;
                    individualTime = transport.TravelDistance / transport.Speed;
                    individualTime = individualTime + transport.DepartureTime / 60;
                    time = time + individualTime;
                }
            }
            time = Math.Round(time,1);
            return time;
        }
        public int AddTransport(string chosenTransport, double distance)
        {
            int status = 0;
            switch (chosenTransport)
            {
                case "1":
                    Higer higer = new Higer(distance);
                    Transports.Add(higer);
                    status = 1;
                    break;
                case "2":
                    FordTransit fordTransit = new FordTransit(distance);
                    Transports.Add(fordTransit);
                    status = 1;
                    break;
                case "3":
                    Sapsan sapsan = new Sapsan(distance);
                    sapsan.CountSpeedLoss();
                    Transports.Add(sapsan);
                    status = 1;
                    break;
                case "4":
                    Lastochka lastochka = new Lastochka(distance);
                    lastochka.CountSpeedLoss();
                    Transports.Add(lastochka);
                    status = 1;
                    break;
                case "5":
                    AirbusA321 airbusA321 = new AirbusA321(distance);
                    if (airbusA321.IsAvailible() == 0)
                    {
                        Transports.Add(airbusA321);
                        status = 1;
                    }
                    else
                        status = airbusA321.IsAvailible();
                    break;
                case "6":
                    EmbraerEMB120 embaraerEMB120 = new EmbraerEMB120(distance);
                    if(embaraerEMB120.IsAvailible() == 0)
                    {
                        Transports.Add(embaraerEMB120);
                        status = 1;
                    }
                    else
                        status = embaraerEMB120.IsAvailible();
                    break;
            }
            return status;
        }
        public bool DeleteTransport(int TransportIndex)
        {
            try
            {
                Transports.RemoveAt(TransportIndex - 1);
                return true;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                return false;
            }
        }
    }
}
