﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class FordTransit : Car
    {
        public FordTransit(double distance)
        {
            TravelDistance = distance;
            TransportName = "Ford Transit";
            ComfortClass = "Business";
            PricePerKm = 4;
            Speed = 90;
            BaggagePricePerKg = 11;
            FuelTankCapacityKm = 150;
            CarClass = "minibus";
        }
        public override double CountTimeToRefuel()
        {
            double OneTimeToRefuel = 4;
            return OneTimeToRefuel * (TravelDistance / FuelTankCapacityKm);
        }
    }
}
