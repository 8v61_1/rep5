﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab3_Routes;

namespace Lab5_Transport
{
    public partial class TransportWinForm : Form
    {
        int routeNo = 0;
        SetOfRoutes setOfRoutes = new SetOfRoutes();
        public TransportWinForm()
        {
            InitializeComponent();
            TransportCBox.Items.Add("Bus Higer");
            TransportCBox.Items.Add("Minibus Ford Transit");
            TransportCBox.Items.Add("Sapsan by Siemens AG");
            TransportCBox.Items.Add("Lastochka by Ural Locomotives");
            TransportCBox.Items.Add("Narrow-body jet airliner AirbusA321");
            TransportCBox.Items.Add("Regional airliner EmbaraerEMB120");
        }

        private void setDistanceAtoB_Click(object sender, EventArgs e)
        {
            int result = 0;
            int.TryParse(textBoxAtoB.Text, out result);
            if (result > 0)
            {
                setOfRoutes.DistanceAB = result;
                textBoxAtoB.Enabled = false;
                setDistanceAtoB.Enabled = false;
                SetBaggageBtn.Enabled = true;
                BaggageTextBox.Enabled = true;
            }
            
        }

        private void AddRouteBtn_Click(object sender, EventArgs e)
        {
            setOfRoutes.AddRoute();
            RouteCBox.Items.Clear();
            for (int i = 0; i < setOfRoutes.Routes.Count; i++)
            {
                RouteCBox.Items.Add("Route "+(i+1));
            }
            RouteCBox.Enabled = true;
            RouteCBox.SelectedIndex = setOfRoutes.Routes.Count-1;
        }

        private void SetBaggageBtn_Click(object sender, EventArgs e)
        {
            if (setDistanceAtoB.Enabled == false)
                AddRouteBtn.Enabled = true;
            double result = 0;
            bool tryParseDone = double.TryParse(BaggageTextBox.Text, out result);
            if ((result >= 0) && (tryParseDone))
            {
                setOfRoutes.BaggageKg = result;
            }
            else
            {
                setOfRoutes.BaggageKg = 0;
                BaggageTextBox.Text = "incorrect value, set to 0";
            }
            if (setOfRoutes.Routes.Count > 0)
            {
                double neededDistance = setOfRoutes.DistanceAB;
                foreach (AbstractTransport abstractTransport in setOfRoutes.Routes[routeNo - 1].Transports)
                {
                    neededDistance = neededDistance - abstractTransport.TravelDistance;
                }
                labelNeededDistance.Text = "";
                labelNeededDistance.Text = "Needed distance:" + neededDistance.ToString();
            }
        }

        private void ChooseTransportBtn_Click(object sender, EventArgs e)
        {
            double neededDistance = setOfRoutes.DistanceAB;
            int routeChosen = RouteCBox.SelectedIndex;
            string transportChosen = (TransportCBox.SelectedIndex + 1).ToString();
            double distance = 0;
            double.TryParse(TransDistBox.Text, out distance);
            if (distance > 0)
            {
                if (setOfRoutes.Routes[routeChosen].AddTransport(transportChosen, distance) != 1)
                {
                    int minimum = setOfRoutes.Routes[routeChosen].AddTransport(transportChosen, distance);
                    string message = "Travel distance is too short for this transport: " + minimum + " is minimum";
                    MessageBox.Show(message, "", MessageBoxButtons.OK);
                }
                else
                {
                    TransportListBox.Enabled = true;

                    TransportListBox.Items.Clear();
                    foreach (AbstractTransport abstractTransport in setOfRoutes.Routes[routeNo - 1].Transports)
                    {
                        TransportListBox.Items.Add(abstractTransport.TransportName +
                                "| Distance: " + abstractTransport.TravelDistance +
                                "| Price per Km: " + abstractTransport.PricePerKm +
                                "| Comfort Class: " + abstractTransport.ComfortClass +
                                "| Speed: " + abstractTransport.Speed);
                        neededDistance = neededDistance - abstractTransport.TravelDistance;
                    }
                    labelNeededDistance.Text = "Needed distance:" + neededDistance.ToString();
                    DeleteTransportBtn.Enabled = true;
                }
            }
            else
                TransDistBox.Text = "incorrect value";
        }

        private void RouteCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RouteCBox.Items.Count==1)
                DeleteRouteBtn.Enabled = false;
            else
                DeleteRouteBtn.Enabled = true;

            double neededDistance = setOfRoutes.DistanceAB;
            string str = RouteCBox.Text;
            routeNo = Convert.ToInt32(str.Remove(0, 6));
            TransportCBox.Enabled = true;

            TransportListBox.Items.Clear();
            foreach (AbstractTransport abstractTransport in setOfRoutes.Routes[routeNo - 1].Transports)
            {
                TransportListBox.Items.Add(abstractTransport.TransportName +
                        "| Distance: " + abstractTransport.TravelDistance +
                        "| Price per Km: " + abstractTransport.PricePerKm +
                        "| Comfort Class: " + abstractTransport.ComfortClass +
                        "| Speed: " + abstractTransport.Speed);
                neededDistance = neededDistance - abstractTransport.TravelDistance;
            }
            labelNeededDistance.Text = "Needed distance:" + neededDistance.ToString();
            if (setOfRoutes.Routes[routeNo - 1].Transports.Count > 0)
                DeleteTransportBtn.Enabled = true;
            else
                DeleteTransportBtn.Enabled = false;
        }

        private void TransportCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            TransDistBox.Enabled = true;
        }

        private void TransDistBox_TextChanged(object sender, EventArgs e)
        {
            ChooseTransportBtn.Enabled = true;
        }

        private void labelNeededDistance_TextChanged(object sender, EventArgs e)
        {
            if (labelNeededDistance.Text == "Needed distance:0")
            {
                labelRoutePrice.Text = "Price(rub): " + setOfRoutes.Routes[routeNo - 1].CountPrice(setOfRoutes.BaggageKg).ToString();
                labelRouteTime.Text = "Time(h): " + setOfRoutes.Routes[routeNo - 1].CountTime().ToString();
            }
            else
            {
                labelRoutePrice.Text = "Price(rub):";
                labelRouteTime.Text = "Time(h):";
            }
        }

        private void DeleteTransportBtn_Click(object sender, EventArgs e)
        {
           int index = TransportListBox.SelectedIndex + 1;
           setOfRoutes.Routes[routeNo - 1].DeleteTransport(index);

            TransportListBox.Items.Clear();
            double neededDistance = setOfRoutes.DistanceAB;
            foreach (AbstractTransport abstractTransport in setOfRoutes.Routes[routeNo - 1].Transports)
            {
                TransportListBox.Items.Add(abstractTransport.TransportName +
                        "| Distance: " + abstractTransport.TravelDistance +
                        "| Price per Km: " + abstractTransport.PricePerKm +
                        "| Comfort Class: " + abstractTransport.ComfortClass +
                        "| Speed: " + abstractTransport.Speed);
                neededDistance = neededDistance - abstractTransport.TravelDistance;
            }
            labelNeededDistance.Text = "Needed distance:" + neededDistance.ToString();
            if (setOfRoutes.Routes[routeNo - 1].Transports.Count > 0)
                DeleteTransportBtn.Enabled = true;
            else
                DeleteTransportBtn.Enabled = false;

        }

        private void DeleteRouteBtn_Click(object sender, EventArgs e)
        {
            int index = routeNo;
            setOfRoutes.DeleteRoute(index);
            RouteCBox.Items.RemoveAt(index-1);
            routeNo = 1;
            RouteCBox.Items.Clear();
            for (int i = 0; i < setOfRoutes.Routes.Count; i++)
            {
                RouteCBox.Items.Add("Route " + (i + 1));
            }
            RouteCBox.SelectedIndex = 0;
        }
    }
}
