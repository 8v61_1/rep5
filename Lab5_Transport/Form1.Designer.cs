﻿namespace Lab5_Transport
{
    partial class TransportWinForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransportListBox = new System.Windows.Forms.ListBox();
            this.ListLabel = new System.Windows.Forms.Label();
            this.RouteCBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAtoB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TransportCBox = new System.Windows.Forms.ComboBox();
            this.ChooseTransportBtn = new System.Windows.Forms.Button();
            this.DeleteTransportBtn = new System.Windows.Forms.Button();
            this.DeleteRouteBtn = new System.Windows.Forms.Button();
            this.AddRouteBtn = new System.Windows.Forms.Button();
            this.setDistanceAtoB = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.BaggageTextBox = new System.Windows.Forms.TextBox();
            this.SetBaggageBtn = new System.Windows.Forms.Button();
            this.labelRoutePrice = new System.Windows.Forms.Label();
            this.labelRouteTime = new System.Windows.Forms.Label();
            this.TransDistBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.labelNeededDistance = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TransportListBox
            // 
            this.TransportListBox.Enabled = false;
            this.TransportListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TransportListBox.FormattingEnabled = true;
            this.TransportListBox.HorizontalScrollbar = true;
            this.TransportListBox.ItemHeight = 17;
            this.TransportListBox.Location = new System.Drawing.Point(296, 200);
            this.TransportListBox.Name = "TransportListBox";
            this.TransportListBox.Size = new System.Drawing.Size(563, 225);
            this.TransportListBox.TabIndex = 0;
            // 
            // ListLabel
            // 
            this.ListLabel.AutoSize = true;
            this.ListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ListLabel.Location = new System.Drawing.Point(412, 164);
            this.ListLabel.Name = "ListLabel";
            this.ListLabel.Size = new System.Drawing.Size(239, 20);
            this.ListLabel.TabIndex = 1;
            this.ListLabel.Text = "Set of transports for this route:";
            // 
            // RouteCBox
            // 
            this.RouteCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RouteCBox.Enabled = false;
            this.RouteCBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RouteCBox.FormattingEnabled = true;
            this.RouteCBox.Location = new System.Drawing.Point(297, 62);
            this.RouteCBox.Name = "RouteCBox";
            this.RouteCBox.Size = new System.Drawing.Size(562, 28);
            this.RouteCBox.TabIndex = 2;
            this.RouteCBox.SelectedIndexChanged += new System.EventHandler(this.RouteCBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(513, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Chosen route:";
            // 
            // textBoxAtoB
            // 
            this.textBoxAtoB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxAtoB.Location = new System.Drawing.Point(13, 124);
            this.textBoxAtoB.Name = "textBoxAtoB";
            this.textBoxAtoB.Size = new System.Drawing.Size(266, 26);
            this.textBoxAtoB.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(47, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Distance from A to B(Km):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(76, 349);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Chose transport:";
            // 
            // TransportCBox
            // 
            this.TransportCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransportCBox.Enabled = false;
            this.TransportCBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TransportCBox.FormattingEnabled = true;
            this.TransportCBox.Location = new System.Drawing.Point(12, 381);
            this.TransportCBox.Name = "TransportCBox";
            this.TransportCBox.Size = new System.Drawing.Size(267, 28);
            this.TransportCBox.TabIndex = 2;
            this.TransportCBox.SelectedIndexChanged += new System.EventHandler(this.TransportCBox_SelectedIndexChanged);
            // 
            // ChooseTransportBtn
            // 
            this.ChooseTransportBtn.Enabled = false;
            this.ChooseTransportBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChooseTransportBtn.Location = new System.Drawing.Point(49, 484);
            this.ChooseTransportBtn.Name = "ChooseTransportBtn";
            this.ChooseTransportBtn.Size = new System.Drawing.Size(192, 34);
            this.ChooseTransportBtn.TabIndex = 5;
            this.ChooseTransportBtn.Text = "Add";
            this.ChooseTransportBtn.UseVisualStyleBackColor = true;
            this.ChooseTransportBtn.Click += new System.EventHandler(this.ChooseTransportBtn_Click);
            // 
            // DeleteTransportBtn
            // 
            this.DeleteTransportBtn.Enabled = false;
            this.DeleteTransportBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteTransportBtn.Location = new System.Drawing.Point(652, 442);
            this.DeleteTransportBtn.Name = "DeleteTransportBtn";
            this.DeleteTransportBtn.Size = new System.Drawing.Size(207, 51);
            this.DeleteTransportBtn.TabIndex = 5;
            this.DeleteTransportBtn.Text = "Delete transport from list";
            this.DeleteTransportBtn.UseVisualStyleBackColor = true;
            this.DeleteTransportBtn.Click += new System.EventHandler(this.DeleteTransportBtn_Click);
            // 
            // DeleteRouteBtn
            // 
            this.DeleteRouteBtn.Enabled = false;
            this.DeleteRouteBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteRouteBtn.Location = new System.Drawing.Point(721, 105);
            this.DeleteRouteBtn.Name = "DeleteRouteBtn";
            this.DeleteRouteBtn.Size = new System.Drawing.Size(138, 34);
            this.DeleteRouteBtn.TabIndex = 5;
            this.DeleteRouteBtn.Text = "Delete Route";
            this.DeleteRouteBtn.UseVisualStyleBackColor = true;
            this.DeleteRouteBtn.Click += new System.EventHandler(this.DeleteRouteBtn_Click);
            // 
            // AddRouteBtn
            // 
            this.AddRouteBtn.Enabled = false;
            this.AddRouteBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddRouteBtn.Location = new System.Drawing.Point(297, 105);
            this.AddRouteBtn.Name = "AddRouteBtn";
            this.AddRouteBtn.Size = new System.Drawing.Size(158, 34);
            this.AddRouteBtn.TabIndex = 5;
            this.AddRouteBtn.Text = "Add Route";
            this.AddRouteBtn.UseVisualStyleBackColor = true;
            this.AddRouteBtn.Click += new System.EventHandler(this.AddRouteBtn_Click);
            // 
            // setDistanceAtoB
            // 
            this.setDistanceAtoB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.setDistanceAtoB.Location = new System.Drawing.Point(49, 164);
            this.setDistanceAtoB.Name = "setDistanceAtoB";
            this.setDistanceAtoB.Size = new System.Drawing.Size(192, 34);
            this.setDistanceAtoB.TabIndex = 5;
            this.setDistanceAtoB.Text = "Set";
            this.setDistanceAtoB.UseVisualStyleBackColor = true;
            this.setDistanceAtoB.Click += new System.EventHandler(this.setDistanceAtoB_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(110, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Baggage(Kg):";
            // 
            // BaggageTextBox
            // 
            this.BaggageTextBox.Enabled = false;
            this.BaggageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BaggageTextBox.Location = new System.Drawing.Point(13, 250);
            this.BaggageTextBox.Name = "BaggageTextBox";
            this.BaggageTextBox.Size = new System.Drawing.Size(266, 26);
            this.BaggageTextBox.TabIndex = 4;
            // 
            // SetBaggageBtn
            // 
            this.SetBaggageBtn.Enabled = false;
            this.SetBaggageBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SetBaggageBtn.Location = new System.Drawing.Point(49, 293);
            this.SetBaggageBtn.Name = "SetBaggageBtn";
            this.SetBaggageBtn.Size = new System.Drawing.Size(192, 34);
            this.SetBaggageBtn.TabIndex = 5;
            this.SetBaggageBtn.Text = "Set";
            this.SetBaggageBtn.UseVisualStyleBackColor = true;
            this.SetBaggageBtn.Click += new System.EventHandler(this.SetBaggageBtn_Click);
            // 
            // labelRoutePrice
            // 
            this.labelRoutePrice.AutoSize = true;
            this.labelRoutePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRoutePrice.Location = new System.Drawing.Point(293, 483);
            this.labelRoutePrice.Name = "labelRoutePrice";
            this.labelRoutePrice.Size = new System.Drawing.Size(106, 20);
            this.labelRoutePrice.TabIndex = 6;
            this.labelRoutePrice.Text = "Price(RUB): ";
            // 
            // labelRouteTime
            // 
            this.labelRouteTime.AutoSize = true;
            this.labelRouteTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRouteTime.Location = new System.Drawing.Point(293, 514);
            this.labelRouteTime.Name = "labelRouteTime";
            this.labelRouteTime.Size = new System.Drawing.Size(72, 20);
            this.labelRouteTime.TabIndex = 6;
            this.labelRouteTime.Text = "Time(h):";
            // 
            // TransDistBox
            // 
            this.TransDistBox.Enabled = false;
            this.TransDistBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TransDistBox.Location = new System.Drawing.Point(13, 442);
            this.TransDistBox.Name = "TransDistBox";
            this.TransDistBox.Size = new System.Drawing.Size(266, 26);
            this.TransDistBox.TabIndex = 4;
            this.TransDistBox.TextChanged += new System.EventHandler(this.TransDistBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(65, 419);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(191, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Transport distance(Km):";
            // 
            // labelNeededDistance
            // 
            this.labelNeededDistance.AutoSize = true;
            this.labelNeededDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNeededDistance.Location = new System.Drawing.Point(293, 448);
            this.labelNeededDistance.Name = "labelNeededDistance";
            this.labelNeededDistance.Size = new System.Drawing.Size(181, 20);
            this.labelNeededDistance.TabIndex = 6;
            this.labelNeededDistance.Text = "Needed distance(Km): ";
            this.labelNeededDistance.TextChanged += new System.EventHandler(this.labelNeededDistance_TextChanged);
            // 
            // TransportWinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 569);
            this.Controls.Add(this.labelRouteTime);
            this.Controls.Add(this.labelNeededDistance);
            this.Controls.Add(this.labelRoutePrice);
            this.Controls.Add(this.AddRouteBtn);
            this.Controls.Add(this.DeleteRouteBtn);
            this.Controls.Add(this.DeleteTransportBtn);
            this.Controls.Add(this.SetBaggageBtn);
            this.Controls.Add(this.setDistanceAtoB);
            this.Controls.Add(this.TransDistBox);
            this.Controls.Add(this.BaggageTextBox);
            this.Controls.Add(this.ChooseTransportBtn);
            this.Controls.Add(this.textBoxAtoB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TransportCBox);
            this.Controls.Add(this.RouteCBox);
            this.Controls.Add(this.ListLabel);
            this.Controls.Add(this.TransportListBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TransportWinForm";
            this.Text = "Transport";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox TransportListBox;
        private System.Windows.Forms.Label ListLabel;
        private System.Windows.Forms.ComboBox RouteCBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAtoB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox TransportCBox;
        private System.Windows.Forms.Button ChooseTransportBtn;
        private System.Windows.Forms.Button DeleteTransportBtn;
        private System.Windows.Forms.Button DeleteRouteBtn;
        private System.Windows.Forms.Button AddRouteBtn;
        private System.Windows.Forms.Button setDistanceAtoB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BaggageTextBox;
        private System.Windows.Forms.Button SetBaggageBtn;
        private System.Windows.Forms.Label labelRoutePrice;
        private System.Windows.Forms.Label labelRouteTime;
        private System.Windows.Forms.TextBox TransDistBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelNeededDistance;
    }
}

