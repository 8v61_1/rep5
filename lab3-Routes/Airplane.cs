﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class Airplane : AbstractLongDistanceTransport
    {
        private string _planeSizeType;

        public string PlaneSizeType
        {
            get { return _planeSizeType; }
            set { _planeSizeType = value; }
        }
        public int IsAvailible()
        {
            if (MinimumDistance > TravelDistance)
                return Convert.ToInt32(MinimumDistance);
            else
                return 0;
        }
    }
}
