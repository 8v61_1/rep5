﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class AbstractTransport
    {
        private string _transportName;
        private string _comfortClass;
        private double _pricePerKm;
        private double _speed;
        private double _baggagePricePerKg;
        private double _travelDistance;

        public string TransportName
        {
            get { return _transportName; }
            set { _transportName = value; }
        }
        public string ComfortClass
        {
            get { return _comfortClass; }
            set { _comfortClass = value; }
        }
        public double PricePerKm
        {
            get { return _pricePerKm; }
            set { _pricePerKm = value; }
        }
        public double Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }
        public double BaggagePricePerKg
        {
            get { return _baggagePricePerKg; }
            set { _baggagePricePerKg = value; }
        }
        public double TravelDistance
        {
            get { return _travelDistance; }
            set { _travelDistance = value; }
        }
    }
}
