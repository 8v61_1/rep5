﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    class Program
    {
        static void Main(string[] args)
        {
            SetOfRoutes setOfRoutes = new SetOfRoutes();
            ConsoleInterface consoleInterface = new ConsoleInterface();
            consoleInterface.NewRoute(setOfRoutes);
            Console.ReadLine();
        }
    }
}
