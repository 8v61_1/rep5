﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    class ConsoleInterface
    {
        string key;
        bool isTransportsChosen = false;
        bool isRouteConfirmed = false;
        int routeNo = 0;
        public void NewRoute(SetOfRoutes setOfRoutes)//Новый путь
        {
            setOfRoutes.AddRoute();
            routeNo++;
            isRouteConfirmed = false;
            if (isTransportsChosen == false)
                EnterDistance(setOfRoutes);
            else
                ChooseTransports(setOfRoutes);
        }
        public void EnterDistance(SetOfRoutes setOfRoutes)
        {
            double distance = 0;
            bool isConvertSuccesfull = false;
            while ((isConvertSuccesfull == false) || (distance <= 0))
            {
                Console.WriteLine("Enter distance from A to B");
                key = Console.ReadLine();
                Console.WriteLine();
                isConvertSuccesfull = Double.TryParse(key, out distance);
                if ((isConvertSuccesfull == false) || (distance <= 0))//проверка на корректность введённой дистанции
                {
                    Console.WriteLine("Invalid distance value, try again\n");
                }
            }
            setOfRoutes.DistanceAB = distance;
            if (isTransportsChosen == false)
                EnterBaggage(setOfRoutes);
            else
                ShowSummary(setOfRoutes);
        }//Исходная дистанция между A и B
        public void EnterBaggage(SetOfRoutes setOfRoutes)
        {
            double baggage = 0;
            bool isConvertSuccesfull = false;
            while ((isConvertSuccesfull == false) || (baggage < 0))
            {
                Console.WriteLine("Enter the baggage weight(kg):");
                key = Console.ReadLine();
                Console.WriteLine();
                isConvertSuccesfull = Double.TryParse(key, out baggage);
                if ((isConvertSuccesfull == false) || (baggage < 0))//проверка на корректность введённой дистанции
                {
                    Console.WriteLine("Invalid baggage value, try again\n");
                }
            }
            setOfRoutes.BaggageKg = baggage;
            if (isTransportsChosen == false)
                ChooseTransports(setOfRoutes);
            else
                ShowSummary(setOfRoutes);
        }//Багаж
        public void ChooseTransports(SetOfRoutes setOfRoutes)
        {
            string transportChosen = "";
            double totalDistance = 0;//дистанция в листе Transports
            int distance = 0;//Дистанция с консоли отдельного транспорта
            bool isConvertSuccesfull = false;
            bool isAddSuccesfull = false;
            if(isTransportsChosen==true)
            {
                foreach (AbstractTransport abstractTransport in setOfRoutes.Routes[routeNo-1].Transports)
                   totalDistance = totalDistance + abstractTransport.TravelDistance;
            }
            while (isAddSuccesfull == false)
            {
                if (transportChosen == "")
                {
                    Console.WriteLine("Add transport:" +
                    "\n1-Bus Higer" +
                    "\n2-Minibus Ford Transit" +
                    "\n3-Sapsan by Siemens AG" +
                    "\n4-Lastochka by Ural Locomotives" +
                    "\n5-Narrow-body jet airliner AirbusA321" +
                    "\n6-Regional airliner EmbaraerEMB120");
                    key = Console.ReadKey().KeyChar.ToString();
                    Console.WriteLine("\n");
                    if (!(key == "1" || key == "2" || key == "3" || key == "4" || key == "5" || key == "6"))//проверка на корректность данных выбора
                    {
                        Console.WriteLine("This option is not in the list, make a choice again \n");
                    }
                    else
                        transportChosen = key;
                }
                if (transportChosen != "")
                {
                    Console.WriteLine("Enter transport travel distance(Km):");
                    key = Console.ReadLine();
                    Console.WriteLine("");
                    isConvertSuccesfull = Int32.TryParse(key, out distance);
                    if ((isConvertSuccesfull == false) || !(distance > 0))//проверка на корректность введённой дистанции
                        Console.WriteLine("Invalid distance value, try again\n");
                    else
                    {
                        if ((distance > setOfRoutes.DistanceAB) || ((totalDistance + distance) > setOfRoutes.DistanceAB))//проверка на итоговую дистанцию, больше ли она исходной
                        {
                            Console.WriteLine("Distance value is bigger than total distance {0}, try again", setOfRoutes.DistanceAB);
                            Console.WriteLine("Your transports distance is now {0}\n",totalDistance);
                        }
                        else
                        {
                            if (setOfRoutes.Routes[routeNo-1].AddTransport(transportChosen, distance) == 1)
                            {
                                isAddSuccesfull = true;
                                totalDistance = totalDistance + distance;
                            }
                            if (isAddSuccesfull == false)//проверка на корректность дистанции
                            {
                                Console.WriteLine("Travel distance is too short for this transport: {0} is minimum\n", setOfRoutes.Routes[routeNo-1].AddTransport(transportChosen, distance));
                                transportChosen = "";
                            }
                        }
                    }
                }
            }
            isTransportsChosen = true;//переместить дальше
            if (setOfRoutes.DistanceAB != totalDistance)
            {
                Console.WriteLine("Add another transport to match your total distance {0}", setOfRoutes.DistanceAB);
                Console.WriteLine("Your transports distance is now {0}\n", totalDistance);
                ChooseTransports(setOfRoutes);
            }
            else
                ShowSummary(setOfRoutes);
        }
        public void ShowSummary(SetOfRoutes setOfRoutes)//Статистика
        {
            string selection = "";
            int route = 1;
            if(setOfRoutes.Routes.Count() == 0)
                Console.WriteLine("No routes");
            else
                Console.WriteLine("Set of routes:");
            foreach (SetOfTransports setOfTransports in setOfRoutes.Routes)
            {
                int i = 1;
                Console.WriteLine("\nRoute {0}", route);
                foreach (AbstractTransport abstractTransport in setOfRoutes.Routes[route-1].Transports)
                {
                    Console.WriteLine(i + "|" + abstractTransport.TransportName +
                        "| Distance: " + abstractTransport.TravelDistance +
                        "| Price per Km: " + abstractTransport.PricePerKm +
                        "| Comfort Class: " + abstractTransport.ComfortClass +
                        "| Speed: " + abstractTransport.Speed);
                    i++;
                }
                Console.WriteLine("Price(rub): " + setOfRoutes.Routes[route-1].CountPrice(setOfRoutes.BaggageKg).ToString());
                Console.WriteLine("Time(h): " + setOfRoutes.Routes[route-1].CountTime().ToString());
                route++;
            }
            if (isRouteConfirmed == false)
            {
                Console.WriteLine("\n1-Delete Transport");
                Console.WriteLine("2-Confirm Route {0}", routeNo);
            }
            else
                selection = "2";
            while (selection == "")
            {
                key = Console.ReadKey().KeyChar.ToString();
                Console.WriteLine("\n");
                if (!(key == "1" || key == "2"))
                {
                    Console.WriteLine("Enter the correct number\n");
                }
                else
                    selection = key;
            }
            switch (selection)
            {
                case "1":
                    DeleteTransport(setOfRoutes);
                    break;
                case "2":
                    isRouteConfirmed = true;
                    Console.WriteLine("1-Edit baggage weight");
                    Console.WriteLine("2-Add route");
                    Console.WriteLine("3-Delete route");
                    selection = "";
                    while (selection == "")
                    {
                        key = Console.ReadKey().KeyChar.ToString();
                        Console.WriteLine("\n");
                        if (!(key == "1" || key == "2" || key == "3" || key == "4"))
                        {
                            Console.WriteLine("Enter the correct number\n");
                        }
                        else
                            selection = key;
                    }
                    switch (selection)
                    {
                        case "1":
                            EnterBaggage(setOfRoutes);
                            break;
                        case "2":
                            NewRoute(setOfRoutes);
                            break;
                        case "3":
                            DeleteRoute(setOfRoutes);
                            break;
                    }
                    break;
            }
        }
        public void DeleteTransport(SetOfRoutes setOfRoutes)
        {
            bool isDeleteSuccesfull = false;
            while (isDeleteSuccesfull == false)
            {
                int index = 0;
                bool isConvertSuccesfull = false;
                Console.WriteLine("Enter the transport number to delete\n");
                while ((isConvertSuccesfull == false) || (index < 0))
                {
                    key = Console.ReadKey().KeyChar.ToString();
                    Console.WriteLine("");
                    isConvertSuccesfull = Int32.TryParse(key, out index);
                    if ((isConvertSuccesfull == false) || (index < 0))
                    {
                        Console.WriteLine("Invalid index value, try again \n");
                    }
                }
                isDeleteSuccesfull = setOfRoutes.Routes[routeNo-1].DeleteTransport(index);
                if (isDeleteSuccesfull == false)
                {
                    Console.WriteLine("Invalid index value, try again \n");
                }
            }
            Console.WriteLine("To match to your total distance {0} add another transport\n",setOfRoutes.DistanceAB);
            ChooseTransports(setOfRoutes);
        }
        public void DeleteRoute(SetOfRoutes setOfRoutes)
        {
            bool isDeleteSuccesfull = false;
            if (setOfRoutes.Routes.Count == 0)
            {
                Console.WriteLine("There is nothing to delete");
            }
            else
            {
                while (isDeleteSuccesfull == false)
                {
                    int index = 0;
                    bool isConvertSuccesfull = false;
                    Console.WriteLine("Enter the route number to delete\n");
                    while ((isConvertSuccesfull == false) || (index < 0))
                    {
                        key = Console.ReadKey().KeyChar.ToString();
                        Console.WriteLine("");
                        isConvertSuccesfull = Int32.TryParse(key, out index);
                        if ((isConvertSuccesfull == false) || (index < 0))
                        {
                            Console.WriteLine("Invalid index value, try again \n");
                        }
                    }
                    isDeleteSuccesfull = setOfRoutes.DeleteRoute(index);
                    if (isDeleteSuccesfull == false)
                    {
                        Console.WriteLine("Invalid index value, try again \n");
                    }
                }
                routeNo--;
            }
            ShowSummary(setOfRoutes);
        }
    }
}
