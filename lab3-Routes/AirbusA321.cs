﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class AirbusA321 : Airplane
    {
        public AirbusA321(double distance)
        {
            TravelDistance = distance;
            TransportName = "AirbusA321";
            ComfortClass = "Economy";
            PricePerKm = 6;
            Speed = 828;
            BaggagePricePerKg = 15;
            MinimumDistance = 500;
            DepartureTime = 40;
            PlaneSizeType = "Narrow-body jet airliner";
        }
    }
}
