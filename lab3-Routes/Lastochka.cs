﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    class Lastochka : Train
    {
        public Lastochka(double distance)
        {
            TravelDistance = distance;
            TransportName = "Lastochka";
            ComfortClass = "Economy";
            PricePerKm = 3;
            Speed = 110;
            BaggagePricePerKg = 5;
            Manufacturer = "Ural Locomotives";
            NumberOfTrainCars = 5;
            MinimumDistance = 200;
            DepartureTime = 20;
        }
    }
}
