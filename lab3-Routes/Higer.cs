﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class Higer : Car
    {
        public Higer(double distance)
        {
            TravelDistance = distance;
            TransportName = "Higer";
            ComfortClass = "Economy";
            PricePerKm = 2.8;
            Speed = 70;
            BaggagePricePerKg = 8;
            FuelTankCapacityKm = 200;
            CarClass = "Bus";
        }
        public override double CountTimeToRefuel()
        {
            double OneTimeToRefuel = 5;
            return OneTimeToRefuel * (TravelDistance / FuelTankCapacityKm);
        }
    }
}
