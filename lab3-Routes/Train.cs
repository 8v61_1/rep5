﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class Train : AbstractLongDistanceTransport
    {
        private string _manufacturer;
        private double _numberOfTrainCars;

        public string Manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }
        public double NumberOfTrainCars
        {
            get { return _numberOfTrainCars; }
            set { _numberOfTrainCars = value; }
        }
        public void CountSpeedLoss()
        {
            Speed = Speed - NumberOfTrainCars * 2;
        }
    }
}
