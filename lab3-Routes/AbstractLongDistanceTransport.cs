﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace lab3_Routes
{
    public class AbstractLongDistanceTransport : AbstractTransport
    {
        private double _departureTime;
        private double _minimumDistance;

        public double DepartureTime
        {
            get { return _departureTime; }
            set { _departureTime = value; }
        }
        public double MinimumDistance
        {
            get { return _minimumDistance; }
            set { _minimumDistance = value; }
        }
    }
}
