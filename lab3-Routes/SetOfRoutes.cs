﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_Routes
{
    public class SetOfRoutes
    {
        private double _distanceAB;
        private double _baggageKg;
        private List<SetOfTransports> _routes = new List<SetOfTransports>();
        
        public double BaggageKg
        {
            get { return _baggageKg; }
            set { _baggageKg = value; }
        }
        public double DistanceAB
        {
            get { return _distanceAB; }
            set { _distanceAB = value; }
        }
        public List<SetOfTransports> Routes
        {
            get { return _routes; }
            set { _routes = value; }
        }
        public void AddRoute()
        {
            SetOfTransports setOfTransports = new SetOfTransports();
            Routes.Add(setOfTransports);
        }
        public bool DeleteRoute(int routeIndex)
        {
            try
            {
                Routes.RemoveAt(routeIndex - 1);
                return true;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                return false;
            }
        }
    }
}
